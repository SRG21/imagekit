console.log("Connecting to Database........................");

const mongoose = require('mongoose');

mongoose.connect(`mongodb://localhost/imageDB`, { useNewUrlParser: true });

const db = mongoose.connection;

db.on('error', console.error.bind(console,"Error connecting to the MongoDB"));

db.once('open', function(){
    console.log(`Connected to the Database :: MongoDB`);
});

module.exports = db;