const mongoose = require('mongoose');

const folderSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    isRoot: {
        type: Boolean,
        default: false
    },
    parent:{
        type:mongoose.Schema.Types.ObjectId,
        ref: 'Folder'
    },
    subfolders:[
        {
            type:mongoose.Schema.Types.ObjectId,
            ref: 'Folder'
        }
    ],
    files:[
        {
            type:mongoose.Schema.Types.ObjectId,
            ref: 'File'
        }
    ],
    root_address:{
        type: String,
        required: true
    },
},{
    timestamps: true
});

const Folder = mongoose.model('Folder', folderSchema);
module.exports = Folder;