const Folder = require('./folders');
const File = require('./files');

// Insert a folder at any level
Folder.create({
    name: "Folder1",
    parent: "id of parent",
    root_address: "Address of system root"
});

//list of all files reverse sorted with date

File.find({}).sort('-createdAt').exec(function(err, files){
    // code for response and error handling
});

//Size of a folder

let folder_id= "id of folder to be found";
let files = await File.find({}).populate({
    path: 'folder',
    match: {$eq: {_id: folder_id}}
});
let size =0;
files.map((file)=> {
    size += parseInt(file.size);
});
return size;

// Delete a folder by id
let folder = await Folder.findById(id);
let parentId = folder.parent;
Folder.findById(parentId, function(err, parent){ // deleting from parents sub folder list
    let index = parent.subfolders.indexOf(folder._id);
    parent.subfolders.slice(index, 0);
    parent.save();
})
Folder.deleteOne({_id: folder._id});


//Search by a filename

File.find({name: "file_name"}, function(err, file){
    // code for response and error handling
})

// Find by filename and format

File.find({name: "File1", format: "PNG"}, function(err, file){
    // code for response and error handling
});

// Change name of folder
Folder.find({name: "SubFolder2"}, function(err, folder){
    if(err){
        return;
    }

    folder.name = "NestedFolder2";
    folder.save();
})