const mongoose = require('mongoose');

const fileSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    size: {
        type: Number,
        required: true
    },
    folder:{
        type:mongoose.Schema.Types.ObjectId,
        ref: 'Folder'
    },
    format: {
        type: String,
        enum: ['PNG', 'TXT', 'JPEG']
    }, 
},{
    timestamps: true
});

const File = mongoose.model('File', fileSchema);
module.exports = File;